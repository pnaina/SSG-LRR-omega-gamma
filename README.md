# SSG-LRR-omega-gamma 
# All codes implemented on OpenFOAM-v2006
Complete formulation of RST-based fully turbulent model SSG/LRR-omega is present in 'Base/ssglrr_omega'

RST-based streamwise transition turbulence model, SSG/LRR-omega-gamma. Available in the folder named: 'RAS/gamma_ssglrr_new' 
Model formulation and description can be found in:
Pisharoti, N., Webster, J., & Brizzolara, S. (2022). Reynolds Stress Turbulence Modelling with γ Transition Model. International Journal of Computational Fluid Dynamics, 1-23. DOI: https://doi.org/10.1080/10618562.2022.2070614 .
Kindly cite the above publication when using the code or the turbulence model. 

RST-based crossflow transition tubulence model, SSG/LRR-omega-gamma-CF is available in the folder named: 'RAS/gamma_crossflow_rsm_1

To use all of these models, compile them in the following order:
- Within folder 'RAS/ssglrr_omega, execute 'wmake'
- Within folder 'RAS/gamma_ssglrr_new, execute 'wmake'
- Within folder 'RAS/gamma_crossflow_rsm_1, execute 'wmake'
